package cn.itcast.ssm.controller;

import java.util.List;

import javax.annotation.Resource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import cn.itcast.ssm.pojo.BaseDict;
import cn.itcast.ssm.pojo.Customer;
import cn.itcast.ssm.pojo.QueryVo;
import cn.itcast.ssm.service.BaseDictService;
import cn.itcast.ssm.service.CustomerService;
import cn.itcast.utils.Page;

@Controller
//@RequestMapping("list")
public class CustomerController {

	@Autowired
	private BaseDictService baseDictService;
	
	@Autowired 
	private CustomerService customerService;
	
	@RequestMapping("/list")
	public String list(QueryVo queryVo,Model model){
		
		//客户来源002
		List<BaseDict> fromType = baseDictService.queryBaseDictListByCode("002");
		model.addAttribute("fromType", fromType);
		
		//所属行业001
		List<BaseDict> industryCode = baseDictService.queryBaseDictListByCode("001");
		model.addAttribute("industryType",industryCode);
		
		//客户级别006
		List<BaseDict> levelType = baseDictService.queryBaseDictListByCode("006");
		model.addAttribute("levelType", levelType);
		
		//查询条件回显
		model.addAttribute("custName", queryVo.getCustName());
		model.addAttribute("custSource", queryVo.getCustSource());
		model.addAttribute("custIndustry", queryVo.getCustIndustry());
		model.addAttribute("custLevel", queryVo.getCustLevel());
		
		Page<Customer> page = customerService.queryCustomerPage(queryVo);
		model.addAttribute("page", page);
		return "list";
	}
	
	
	@RequestMapping("/edit")
	@ResponseBody
	public Customer edit(Integer id){
		return customerService.queryCustomerById(id);
	}
	
	@RequestMapping("update")
	@ResponseBody
	public String update(Customer customer){
		customerService.updateCustomerById(customer);
		return "ok";
	}
	
	@RequestMapping("delete")
	@ResponseBody
	public String delete(Integer id){
		customerService.deleteCustomerById(id);
		return "ok";
	}
	
}
