package cn.itcast.ssm.service;

import java.util.List;

import cn.itcast.ssm.pojo.BaseDict;

public interface BaseDictService {
	
	/**
	 * queryBaseDictListByCode   根据code查询字典列表信息
	 * @param code
	 * @return
	 */
	public List<BaseDict> queryBaseDictListByCode(String code);

}
