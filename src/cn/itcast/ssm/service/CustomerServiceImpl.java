package cn.itcast.ssm.service;

import java.util.List;

import javax.annotation.Resource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import cn.itcast.ssm.mapper.CustomerMapper;
import cn.itcast.ssm.pojo.Customer;
import cn.itcast.ssm.pojo.QueryVo;
import cn.itcast.utils.Page;

@Service
public class CustomerServiceImpl implements CustomerService {

	@Autowired
	private CustomerMapper customerMapper;
	
	@Override
	public Page<Customer> queryCustomerPage(QueryVo queryVo) {
		Page<Customer> page = new Page<>();
		if(queryVo.getPage() != null){
			Integer start = (queryVo.getPage() - 1) * queryVo.getSize();
			queryVo.setStart(start);
		}
		
		int count = customerMapper.queryCustomerCount(queryVo);
		List<Customer> rows = customerMapper.queryCustomerList(queryVo);
		page.setSize(queryVo.getSize());
		page.setRows(rows);
		page.setPage(queryVo.getPage());
		page.setTotal(count);
		return page;
	}

	@Override
	public Customer queryCustomerById(Integer id) {
		return customerMapper.queryCustomerById(id);
	}

	@Override
	public void updateCustomerById(Customer customer) {
		customerMapper.updateCustomerById(customer);
	}

	@Override
	public void deleteCustomerById(Integer id) {
		customerMapper.deleteCustomerById(id);
	}

}
