package cn.itcast.ssm.service;

import cn.itcast.ssm.pojo.Customer;
import cn.itcast.ssm.pojo.QueryVo;
import cn.itcast.utils.Page;

public interface CustomerService {
	
	public Page<Customer> queryCustomerPage(QueryVo queryVo);
	
	//根据主键查询
	public Customer queryCustomerById(Integer id);
	
	//根据id更新customer
	public void updateCustomerById(Customer customer);
	
	//删除customer
	public void deleteCustomerById(Integer id);

}
