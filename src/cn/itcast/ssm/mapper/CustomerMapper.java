package cn.itcast.ssm.mapper;

import java.util.List;

import cn.itcast.ssm.pojo.Customer;
import cn.itcast.ssm.pojo.QueryVo;

public interface CustomerMapper {

	
	//结果集
	public List<Customer> queryCustomerList(QueryVo queryVo);
	
	//总记录数
	public int queryCustomerCount(QueryVo queryVo);
	
	//根据主键查询
	public Customer queryCustomerById(Integer id);
	
	//根据主键更新
	public void updateCustomerById(Customer customer);
	
	//删除
	public void deleteCustomerById(Integer id);
}
